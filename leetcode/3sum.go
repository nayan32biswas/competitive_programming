package main

import (
	"fmt"
	"sort"
)

func binary_search_lower(arr []int, key int, left int) int {
	mid := 0
	start := left
	End := len(arr) - 1
	for End-start > 1 {
		mid = (start + End) / 2
		if arr[mid] < key {
			start = mid
		} else {
			End = mid
		}
	}
	if arr[End] <= key {
		return End
	}
	return start
}
func threeSum(nums []int) [][]int {
	var results [][]int
	length := len(nums)
	mark := map[string]bool{}
	if length < 3 {
		return results
	}
	sort.Ints(nums)
	var diff, pos int
	var str string
	for i := 0; i < length; i++ {
		for j := i + 1; j < length-1; j++ {
			diff = 0 - (nums[i] + nums[j])
			pos = binary_search_lower(nums, diff, j+1)
			if pos < length && nums[pos] == diff {
				str = fmt.Sprint(nums[i]) + fmt.Sprint(nums[j]) + fmt.Sprint(nums[pos])
				if ok := mark[str]; !ok {
					results = append(results, []int{nums[i], nums[j], nums[pos]})
				}
				mark[str] = true
			}
		}
	}
	return results
}

func main() {
	nums := []int{-1, 0, 1, 2, -1, -4}
	fmt.Println(threeSum(nums))
}
